from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test

def decorate_dispatch(f):
    def _f(cls):
        cls.dispatch = method_decorator(f)(cls.dispatch)
        return cls
    return _f

def check_groups(u, *group_names):
    if u.is_authenticated():
        if u.groups.filter(name__in=group_names) or u.is_superuser:
            return True
    return False    

def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    def in_groups(u):
        return check_groups(u, *group_names)
    return user_passes_test(in_groups)

