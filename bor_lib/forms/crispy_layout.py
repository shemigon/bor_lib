from django.template.loader import render_to_string

class CrispyTemplate(object):

    def __init__(self, template_name, *fields, **kwargs):
        self.template_name = template_name
        self.fields = list(fields)

    def render(self, form, form_style, context):
        for f in self.fields:
            if not f in form.rendered_fields:
                form.rendered_fields.add(f)
        return render_to_string(self.template_name, context)