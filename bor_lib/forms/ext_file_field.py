# -*- coding: utf-8 -*-

import os

from django import forms


class ExtFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        ext_whitelist = kwargs.pop("ext_whitelist")
        self.ext_whitelist = [i.lower() for i in ext_whitelist]

        super(ExtFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(ExtFileField, self).clean(*args, **kwargs)
        filename = data.name
        ext = os.path.splitext(filename)[1]
        ext = ext.lower()
        if ext not in self.ext_whitelist:
            ext_whitelist_verbose = ", ".join([x[1:] for x in self.ext_whitelist])
            raise forms.ValidationError(u"Не правильный формат файла. Допустимые форматы {0}".format(ext_whitelist_verbose))
        return data