# -*- coding: utf-8 -*-
from django import forms
from django.core.validators import EMPTY_VALUES
from django.forms import ChoiceField, ModelChoiceField, Field
from django.forms.models import ModelChoiceIterator


class EmptyValueChoiceField(ChoiceField):

    def __init__(self, empty_value=None , *args, **kwargs):
        super(EmptyValueChoiceField, self).__init__(*args,**kwargs)
        self.empty_value = [('none', empty_value if empty_value else '-----------')]
        self.choices = self.empty_value + self.choices

    def clean(self, value):
        value = super(EmptyValueChoiceField,self).clean(value)
        if value == "none":
            raise forms.ValidationError('Обязательное поле.')
        return value


class EmptyValueFieldModelChoiceIterator(ModelChoiceIterator):

    def __iter__(self):
        if self.field.empty_label is not None:
            yield self.field.empty_value
        if self.field.cache_choices:
            if self.field.choice_cache is None:
                self.field.choice_cache = [
                    self.choice(obj) for obj in self.queryset.all()
                ]
            for choice in self.field.choice_cache:
                yield choice
        else:
            for obj in self.queryset.all():
                yield self.choice(obj)


class EmptyValueModelChoiceField(ModelChoiceField):

    def __init__(self, empty_value=None, *args, **kwargs):
        self.empty_value = ('none', empty_value if empty_value else '-----------')
        super(EmptyValueModelChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if value == "none":
            return None
        return super(EmptyValueModelChoiceField,self).to_python(value)

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return EmptyValueFieldModelChoiceIterator(self)

    choices = property(_get_choices, ChoiceField._set_choices)
