from django.core.management.base import BaseCommand
from django.template.loader import render_to_string

class Command(BaseCommand):
    args = '<app_name ModelName model_name>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):

        data = {
            'app_name': args[0],
            'ModelName': args[1],
            'model_name': args[2]
        }

        tests = render_to_string('crud/crud_tests.tpl', data)
        views = render_to_string('crud/crud_views.tpl', data)
        urls = render_to_string('crud/crud_urls.tpl', data)
        forms = render_to_string('crud/crud_forms.tpl', data)

        print '\ntests.py\n'
        print '\n'
        print tests

        print '\nviews.py\n'
        print '\n'
        print views

        print '\nurls.py\n'
        print '\n'
        print urls

        print '\nforms.py\n'
        print '\n'
        print forms        