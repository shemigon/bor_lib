# -*- coding: utf-8 -*-

from django.db import models
from django.db.models.query import QuerySet

class SafeDeleteManager(models.Manager):

    def get(self, *args, **kwargs):
        qs = QuerySet(self.model)
        return qs.get(*args, **kwargs)

    def get_query_set(self):
        qs = QuerySet(self.model)
        return qs.filter(is_deleted=False)

    def get_deleted_query_set(self):
        qs = QuerySet(self.model)
        return qs.filter(is_deleted=True)

    def get_deleted(self, *args, **kwargs):
        return self.get_deleted_query_set().get(*args, **kwargs)

    # Фильтры
    def deleted(self):
        return self.filter(is_deleted=True)

class SafeDeleteModel(models.Model):

    is_deleted = models.BooleanField(editable=False, default=False)

    class Meta:
        abstract = True

    objects = SafeDeleteManager()
    all_objects = models.Manager()

    def delete(self):
        self.is_deleted = True
        self.save()