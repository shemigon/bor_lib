from django import forms
from .models import {{ModelName}}

class {{ModelName}}Form(forms.ModelForm):

    class Meta:
        model = {{ModelName}}
        exclude=('user',)