from mapson.common.tests import MapsonTestCase
from django.core.urlresolvers import reverse
from .models import {{ModelName}}
from .factories import {{ModelName}}Factory

class {{ModelName}}TestCase(MapsonTestCase):

    def test_create(self):
        url = reverse('{{app_name}}:{{model_name}}:create')

        data = {
            'name':'name'
        }

        response = self.unlogined_client.get(url)
        self.assertLoginRedirects(response, url)

        response = self.simple_user_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplate(response, '{{app_name}}/{{model_name}}_form.html')

        response = self.simple_user_client.post(url, data)
        self.assertRedirects(response, reverse('{{app_name}}:{{model_name}}:list'))

        {{model_name}} = {{ModelName}}.objects.all()[0]

        self.assertEqual({{model_name}}.name, 'name')


    def test_update(self):

        {{model_name}} = {{ModelName}}Factory.create(user=self.simple_user)
        url = reverse('{{app_name}}:{{model_name}}:update', kwargs={'pk': {{model_name}}.id})

        data = {
            'name':'updated'
        }

        response = self.unlogined_client.get(url)
        self.assertLoginRedirects(response, url)

        response = self.another_user_client.get(url)
        self.assertEqual(response.status_code, 403)

        response = self.simple_user_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplate(response, '{{app_name}}/{{model_name}}_form.html')        

        response = self.simple_user_client.post(url, data)
        self.assertRedirects(response, reverse('{{app_name}}:{{model_name}}:list'))

        {{model_name}} = {{ModelName}}.objects.get(id={{model_name}}.id)

        self.assertEqual({{model_name}}.name, 'updated')

    def test_delete(self):

        {{model_name}} = {{ModelName}}Factory.create(user=self.simple_user)
        url = reverse('{{app_name}}:{{model_name}}:delete', kwargs={'pk': {{model_name}}.id})

        response = self.unlogined_client.post(url)
        self.assertLoginRedirects(response, url)

        response = self.another_user_client.post(url)
        self.assertEqual(response.status_code, 403)

        response = self.simple_user_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplate(response, '{{app_name}}/{{model_name}}_delete.html')         

        response = self.simple_user_client.post(url)
        self.assertRedirects(response, reverse('{{app_name}}:{{model_name}}:list'))

        {{model_name}} = {{ModelName}}.objects.get(id={{model_name}}.id)

        self.assertEqual({{model_name}}.is_deleted, True)

    def test_list(self):

        url = reverse('{{app_name}}:{{model_name}}:list')

        {{model_name}}_1 = {{ModelName}}Factory.create(user=self.simple_user)
        {{model_name}}_2 = {{ModelName}}Factory.create(user=self.simple_user)
        {{model_name}}_3 = {{ModelName}}Factory.create(user=self.another_user)

        response = self.unlogined_client.get(url)
        self.assertLoginRedirects(response, url)

        response = self.simple_user_client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplate(response, '{{app_name}}/{{model_name}}_list.html') 
        self.assertEqual(list(response.context['object_list']), [{{model_name}}_1, {{model_name}}_2])




