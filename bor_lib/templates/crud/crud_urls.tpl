# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from .views import *

{{model_name}}_patterns = patterns('',
    url(r'^create/$', {{ModelName}}CreateView.as_view(), name='create'),
    url(r'^update/(?P<pk>\w+)/$', {{ModelName}}UpdateView.as_view(), name='update'),
    url(r'^delete/(?P<pk>\w+)/$', {{ModelName}}DeleteView.as_view(), name='delete'),
    url(r'^list/$', {{ModelName}}ListView.as_view(), name='list'),
)

urlpatterns = patterns('',
    url(r'^{{model_name}}/', include({{model_name}}_patterns, namespace='{{model_name}}')),
    )