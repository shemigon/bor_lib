from .forms import {{ModelName}}Form
from .models import {{ModelName}}
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.core.urlresolvers import reverse_lazy
from braces.views import LoginRequiredMixin
from bor_lib.views.common import ObjectOwnerMixin
from django.http import HttpResponseRedirect

class {{ModelName}}CreateView(LoginRequiredMixin, CreateView):

    model = {{ModelName}}
    form_class = {{ModelName}}Form
    template_name = '{{app_name}}/{{model_name}}_form.html'
    success_url = reverse_lazy('{{app_name}}:{{model_name}}:list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

class {{ModelName}}UpdateView(LoginRequiredMixin, ObjectOwnerMixin, UpdateView):

    model = {{ModelName}}
    form_class = {{ModelName}}Form
    template_name = '{{app_name}}/{{model_name}}_form.html'
    success_url = reverse_lazy('{{app_name}}:{{model_name}}:list')

class {{ModelName}}DeleteView(LoginRequiredMixin, ObjectOwnerMixin, DeleteView):

    model = {{ModelName}}
    template_name = '{{app_name}}/{{model_name}}_delete.html'
    success_url = reverse_lazy('{{app_name}}:{{model_name}}:list')

class {{ModelName}}ListView(LoginRequiredMixin, ListView):

    model = {{ModelName}}
    template_name = '{{app_name}}/{{model_name}}_list.html'

    def get_queryset(self):
        queryset = super({{ModelName}}ListView, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset
