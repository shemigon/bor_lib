from django import template
from django.contrib.contenttypes.models import ContentType

register = template.Library()

@register.filter
def get_content_type_id( value ):
    app_label, model = value.split('.')
    ct = ContentType.objects.get(app_label = app_label, model = model)
    return ct.id