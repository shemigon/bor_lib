from django import template

register = template.Library()

@register.filter
def getitem( obj, name ):
	try:
		return obj[name]
	except KeyError:
		return None