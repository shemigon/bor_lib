# coding: utf-8

from django import template
register = template.Library()

@register.filter()
def path_without(request, params):
    """Возвращает путь без параметра param.
    Использовать в шаблоне так: href="{{request|path_without|"param_name1, parapm_name2..."}}"
    """
    query_dict = request.GET.copy()
    params_list = [p.strip() for p in params.split(',')]
    for param in params_list:
        if param in query_dict:
            del query_dict[param]
    querystring = query_dict.urlencode()
    path = request.path
    if querystring:
        path = "%s?%s&" % ( path, querystring )
    else: 
        path = "%s?" % path    
    return path