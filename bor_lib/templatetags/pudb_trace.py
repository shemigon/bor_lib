from django import template
import pudb

register = template.Library()

def pudb_trace(context):
    pudb.set_trace()

register.simple_tag(takes_context=True)(pudb_trace)

