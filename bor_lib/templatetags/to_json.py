# -*- coding: utf-8 -*-
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.utils.safestring import mark_safe
from django.template import Library
from django.core.serializers import json


register = Library()


@register.filter
def to_json(obj):
    if isinstance(obj, QuerySet):
        return mark_safe(serialize('json', obj))
    return mark_safe(json.json.dumps(obj, cls=json.DjangoJSONEncoder, sort_keys=True, ensure_ascii=False))