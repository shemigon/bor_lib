# -*- coding: utf-8 -*-

class MaxSequenceMixin(object):

    @classmethod
    def _setup_next_sequence(cls):
        if cls._associated_class.objects.all():
            return cls._associated_class.objects.latest('id').id + 1
        return 0