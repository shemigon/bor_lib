from django.test import TestCase as BaseTestCase

class TestCase(BaseTestCase):

    def assertTemplate(self, response, template_name):
        self.assertTrue(template_name in response.template_name,
            "Template '%s' was not a template name to render"
            " the response. Actual template names: %s" %
                (template_name, ', '.join(response.template_name)))

