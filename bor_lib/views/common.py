from django.http import HttpResponse, HttpResponseForbidden, HttpResponsePermanentRedirect
from django.views.generic.base import View
import json

class LoginRequired403Mixin(object):
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()
        return super(LoginRequired403Mixin, self).dispatch(request, *args,
            **kwargs)

class ObjectOwnerMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()
        self.object = self.get_object()
        if self.object.user != self.request.user:
            return HttpResponseForbidden()
        return super(ObjectOwnerMixin, self).dispatch(request, *args,
            **kwargs)

class EscapedFragmentMixin(object):

    def get(self, request, *args, **kwargs):
        if request.GET.get('_escaped_fragment_', None):
            url = request.GET.get('_escaped_fragment_')
            if url.startswith('/sidetab'):
                url = url[len('/sidetab'):]
            elif url.startswith('/sidetab/narrow'):
                url = url[len('/sidetab/narrow'):]
            return HttpResponsePermanentRedirect(url)
        return super(EscapedFragmentMixin, self).get(request, *args, **kwargs)

class MockView(View):

    def get(self, *args, **kwargs):
        return HttpResponse()

    def post(self, *args, **kwargs):
        return HttpResponse()

