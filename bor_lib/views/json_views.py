# -*- coding: utf-8 -*-

from django.utils import simplejson
from django.http import HttpResponse

class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """

    def render_to_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        is_json = response_kwargs.pop('is_json', False)
        if is_json:
            response_kwargs['content_type'] = 'application/json'
            return HttpResponse(
                self.convert_context_to_json(context),
                **response_kwargs
            )
        return super(JSONResponseMixin, self).render_to_response(context, **response_kwargs)

    def convert_context_to_json(self, context):
        "Convert the context dictionary into a JSON object"
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return simplejson.dumps(context)

class JSONFormMixin(JSONResponseMixin):

    def form_invalid(self, form):
        return self.render_to_response({ 'status' : 'errors', 'errors' : form.errors }, is_json = True)

    def form_valid(self, form):
        form.save()
        return self.render_to_response({ 'status' : 'success'}, is_json = True)

    def one_string_errors(self, form):
        error_message = u''
        for k, v in sorted(form.errors.items()):
            if k == '__all__':
                for error in v:
                    error_message += u'{error}\n'.format(error = error)                    
            else:
                field = form[k]
                field_label = field.label
                for error in v:
                    error_message += u'{field_label}. {error}\n'.format(field_label=field_label, error = error)
        return self.render_to_response({ 'status' : 'errors', 'errors' : form.errors, 'error_message' : error_message }, is_json = True)

class JSONModelFormMixin(JSONFormMixin):

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response({ 'status' : 'success', 'pk' : self.object.pk }, is_json = True)