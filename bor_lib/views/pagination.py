# -*- coding: utf-8 -*-
from endless_pagination.views import PAGE_LABEL

class AjaxPageTemplateMixin(object):

    key = PAGE_LABEL
    page_template = None
    page_template_suffix = '__page'

    def get_page_template(self, **kwargs):
        if self.template_name:
            name, ext = self.template_name.split('.')
            return '{0}{1}.{2}'.format(
                name,
                self.page_template_suffix,
                ext
            )
        return None

    def get_template_names(self):
        """Switch the templates for Ajax requests."""
        request = self.request
        if 'page_only' in request.REQUEST:
            return [self.page_template if self.page_template else self.get_page_template()]
        return super(
            AjaxPageTemplateMixin, self).get_template_names()