# -*- coding: utf-8 -*-
from django.http import HttpResponseForbidden, HttpResponseBadRequest
from bor_lib.decorators import check_groups
from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.utils.encoding import force_text
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth import REDIRECT_FIELD_NAME

class ObjectPermissionsMixin(object):

    permission_object_slug = 'pk'
    redirect = False
    login_url = None
    redirect_field_name = REDIRECT_FIELD_NAME
    foo = 'bar'

    def get_login_url(self):

        login_url = self.login_url or settings.LOGIN_URL
        if not login_url:
            raise ImproperlyConfigured(
                "Define %(cls)s.login_url or settings.LOGIN_URL or override "
                "%(cls)s.get_login_url()." % {"cls": self.__class__.__name__})

        return force_text(login_url)

    def get_redirect_field_name(self):
        """
        Override this method to customize the redirect_field_name.
        """
        if self.redirect_field_name is None:
            raise ImproperlyConfigured(
                "%(cls)s is missing the "
                "redirect_field_name. Define %(cls)s.redirect_field_name or "
                "override %(cls)s.get_redirect_field_name()." % {
                "cls": self.__class__.__name__})    
        return self.redirect_field_name

    def permission_fail(self, request):
        if self.redirect:
            return redirect_to_login(request.get_full_path(),
                              self.get_login_url(),
                              self.get_redirect_field_name())
        else:
            return HttpResponseForbidden()

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return self.permission_fail(request)
        if self.group_required and not check_groups(request.user, self.group_required):
            return self.permission_fail(request)
        if hasattr(self, 'permission') and self.permission:
            obj = None
            if kwargs.get(self.permission_object_slug, None):
                try:
                    obj = self.permission_model.objects.get(pk = kwargs.get(self.permission_object_slug))
                except self.permission_model.DoesNotExist:
                    return HttpResponseBadRequest()
            if not (request.user.has_perm(self.permission) or (obj and request.user.has_perm(self.permission, obj))):
                return self.permission_fail(request)
        return super(ObjectPermissionsMixin, self).dispatch(request, *args, **kwargs) 