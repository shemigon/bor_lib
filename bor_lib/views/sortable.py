from django.views.generic.base import View
import json
from .json_views import JSONResponseMixin


class SortObjects(JSONResponseMixin, View):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.POST['sort_data'])
        object_list = self.get_queryset(data.keys())
        for x in object_list:
            x.order = int(data[str(x.id)])
            x.save()

        return self.render_to_response({'status' : 'success'}, is_json=True)


        
