# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

VERSION = (0, 1, 11)
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))

setup(
    name='bor_lib',
    version=__versionstr__,
    description="Sergey Borodenkov common library",
    author='SevenQuark',
    author_email='info@sevenquark.com',
    url='http://www.sevenquark.com',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires = ['pudb'],
)
